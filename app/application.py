from flask import Flask, render_template, redirect
from flask_pymongo import PyMongo
from pymongo import MongoClient, InsertOne
import os


def create_app():
    app = Flask(__name__)
    if os.environ['FLASK_ENV'] == 'LOCAL':
        app.config['MONGO_URI'] = 'mongodb://localhost:28001/Database'
    if os.environ['FLASK_ENV'] == 'DOCKER':
        app.config['MONGO_URI'] = 'mongodb://mongodb:27017/database'
    return app


def run_environment():
    app = create_app()
    db = PyMongo(app)
    col = db.db.costumers
    set_routes(app, col)
    return app


def set_routes(app, col):
    @app.route('/', methods=['GET'])
    def index():
        count = col.count()
        data = col.find({}, {'_id': 0}).sort('_id')
        names = col.find({}, {'_id': 0, 'Email': 0}).sort('Nome')
        emails = col.find({}, {'_id': 0}).sort('Email')
        return render_template('index.html', data=data,
                                             count=count,
                                             names=names,
                                             emails=emails)

    @app.route('/pulling', methods=['POST'])
    def pulling():
        data =[{
            'Nome': 'Mano Brown',
            'Email': 'negrodrama@outlook.com'
        },
        {
            'Nome': 'Amado Batista',
            'Email': 'modao@yahoo.com'
        },
        {
            'Nome': 'Thor',
            'Email': 'sonofodin@gmail.com'
        },
        {
            'Nome': 'Me',
            'Email': 'donthaveone@outlook.com'
        },
        {
            'Nome': 'Tony Stark',
            'Email': 'sodead@richpeople.com'
        },
        {
            'Nome': 'Batman',
            'Email': 'detective.detective@.com'
        }]
        col.insert(data)
        return redirect("/")

    @app.route('/delete', methods=['POST'])
    def delete():
        col.drop()
        return redirect("/")
