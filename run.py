from app.application import run_environment


if __name__ == '__main__':
    app = run_environment()
    app.run(debug=False, host='0.0.0.0', port=5000)
